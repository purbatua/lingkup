const defaultTheme = require('tailwindcss/defaultTheme')


module.exports = {
  prefix: '',
  important: false,
  theme: {

    extend: {
      fontFamily: {
        sans: [
          '"Open Sans"',
          ...defaultTheme.fontFamily.sans
        ]
      },
      fontSize: {
        '2xs': '.65rem'
      },
    }
  },
  corePlugins: {},
  plugins: [],
}
