/**
 * @source https://www.youtube.com/watch?v=dGlTmR5DzP8 
 * @desc filename pattern: [name].module.js
 */
import camelCase from 'lodash/camelCase'

const requireModule = require.context('.', false, /\.module\.js$/);
const modules = {}


requireModule.keys().forEach(filename => {
  if(filename === './index.js') return;

  const moduleName = camelCase(filename.replace(/(\.\/|\.module\.js)/g, ''))
  modules[moduleName] = requireModule(filename).default
})


export default modules