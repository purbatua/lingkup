import api from '@/common/api.service'
import JwtService from '@/common/jwt.service'
import { LOGIN, LOGOUT, SET_AUTH, SET_ERROR, PURGE_AUTH } from '../types'


const state = {
  user: {},
  isAuthenticated: !!JwtService.getToken(),
  errors: {}
}

const getters = {
  currentUser(state) {
    return state.user
  },
  isAuthenticated(state) {
    return state.isAuthenticated
  }
}

const actions = {
  [LOGIN](context, credentials) {
    return new Promise((resolve, reject) => {
      api.post(`/auth/login`, credentials)
      .then(({data}) => {
        context.commit(SET_AUTH, data)
        resolve(data)
      })
      .catch((response) => {
        context.commit(SET_ERROR, response.data)
        reject(response.data)
      })
    })
  },
  [LOGOUT](context) {
    context.commit(PURGE_AUTH)
  }
}

const mutations = {
  [SET_AUTH](state, user) {
    state.user = user
    state.isAuthenticated = true
    state.errors = {}
    JwtService.saveToken(state.user.token)
  },
  [SET_ERROR](state, error) {
    state.errors = error
  },
  [PURGE_AUTH](state) {
    state.user = {}
    state.isAuthenticated = false
    state.errors = {}
    JwtService.destroyToken();
  }
}


export default { namespaced: true, state, getters, actions, mutations }