import api from '@/common/api.service'
import { FETCH_ARTICLES, SET_ARTICLES, SET_ERROR } from '../types'



const state = {
  articles: [],
  error: {}
}

const getters = {
  articles(state) {
    return state.articles
  },
  popular(state) {
    return state.articles
  }
}

const actions = {
  [FETCH_ARTICLES](context, credentials) {
    console.log(FETCH_ARTICLES + 'called')
    return new Promise((resolve, reject) => {
      api.get(`/users/1/articles`) // credentials
      .then(({data}) => {
        console.log(FETCH_ARTICLES, data)
        context.commit(SET_ARTICLES, data)
        resolve(data)
      })
      .catch((response) => {
        context.commit(SET_ERROR, response.data)
        reject(response.data)
      })
    })
  }
}

const mutations = {
  [SET_ARTICLES](state, articles) {
    state.articles = articles
  },
  [SET_ERROR](state, error) {
    state.error = error
  }
}


export default { namespaced: true, state, getters, actions, mutations }

