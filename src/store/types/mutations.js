export const SET_AUTH = 'SET_AUTH'
export const SET_ERROR = 'SET_ERROR'
export const PURGE_AUTH = 'PURGE_AUTH'
export const SET_ARTICLES = 'SET_ARTICLES'