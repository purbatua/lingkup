export const LOGIN = 'login'
export const LOGOUT = 'logout'
export const REGISTER = 'register'
export const FETCH_ARTICLES = 'fetchArticles'
