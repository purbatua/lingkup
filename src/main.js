import Vue from 'vue'

import store from './store/store'
import './registerServiceWorker'


// Core
import App from './App.vue'
import router from './router'

// Styles
import '@/assets/styles/tailwind.css'

// Global components
import Default from '@/layouts/DefaultLayout.vue'
Vue.component('default-layout', Default)

// Global Filters
import DateFilter from '@/common/date.filter'
Vue.filter('date', DateFilter)


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
