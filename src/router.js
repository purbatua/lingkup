import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import scrollBehavior from './common/scrollBehavior'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: { scrollToTop: true, transitionName: 'fade' }    
    },
    {
      path: '/search',
      name: 'search',
      component: () => import(/* webpackChunkName: "search" */ './views/Search.vue'),
      meta: { layout: 'fullwidth', transitionName: 'slide' }
    },
    {
      path: '/video',
      name: 'video',
      component: () => import(/* webpackChunkName: "video" */ './views/Video.vue')
    },
    {
      path: '/podcast',
      name: 'podcast',
      component: () => import(/* webpackChunkName: "podcast" */ './views/Podcast.vue')
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/:slug',
      name: 'article-detail',
      component: () => import(/* webpackChunkName: "article-detail" */ './views/ArticleDetail.vue'),
      meta: { scrollToTop: true, transitionName: 'fade' }
    },
  ]
})
