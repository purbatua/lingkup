import format from '@/_lib/format'


export default (date, date_format='dd MMMM yyy') => {
  return format(new Date(date), date_format)
}