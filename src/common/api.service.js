import axios from 'axios'
import JwtService from './jwt.service'


const axiosInstance = axios.create()
axiosInstance.defaults.baseURL = `${process.env.VUE_APP_API_URL}`
axiosInstance.defaults.timeout = 2500
axiosInstance.defaults.headers.post['Content-Type'] = 'application/json'
// axiosInstance.defaults.headers.common['Authorization'] = JwtService.getToken()


export default axiosInstance