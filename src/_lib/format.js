import { format } from 'date-fns'
import id from 'date-fns/locale/id'


window.__localeId__ = id

export default function (date, formatStr) {
  return format(date, formatStr, {
    locale: window.__localeId__
  })
}